export interface Persona{
  idpersona:number;
	razonsocial?:string
	cuit:string;
	dni?:string;
	aniofundacion?:number;
	apellido?:string;
	nombre?:string;
	idCategoriaPersona:number
	valorCategoriaPersona:string
  descripcionCategoriaPersona?:string
}
