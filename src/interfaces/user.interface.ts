export interface User{
  username:string;
  password:string;
  email:String;
  name:String;
  enabled:number;
  token:string;
}
