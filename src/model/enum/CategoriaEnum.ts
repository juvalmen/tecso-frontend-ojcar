export const CategoriaEnum = {
  FIS: {
    idcategoria: "1",
    nombre: "Físico",
    valor: 1,
  },
  JUR: {
    idcategoria: "2",
    nombre: "Jurídico",
    valor: 2,
  },
}

