import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MessageService, Message } from "primeng/api";
import { User } from "src/services/user/user.model";
import { UserService } from "src/services/user/user.service";
import { validateConfig } from "@angular/router/src/config";

@Component({
   selector: "app-sign-up",
   templateUrl: "./sign-up.component.html",
   styleUrls: ["./sign-up.component.css"],
   providers: [MessageService]
})
export class SignUpComponent implements OnInit {
   user: User;
   msgs: Message[] = [];
   emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";

   constructor(
      private userService: UserService,
      public messageService: MessageService
   ) {}

   ngOnInit() {
      this.resetForm();
   }

   resetForm(form?: NgForm) {
      if (form != null) form.reset();
      this.user = {
         username: "",
         password: "",
         email: ""
      };
   }

   OnSubmit(form: NgForm) {
      if (!this.validatePassword()) {
         return;
      }
      this.userService.registerUser(form.value).subscribe(
         (data: any) => {
          if (data != "") {
              console.log(data)
               if (data.statusCode == "200") {
                  this.resetForm(form);
                  this.addMessage("", "Usuario registrado con exito.", "info");
               } else {
                  this.addMessage("", data.responseMessage, "warn");
               }
            }
         },
         error => {
            this.addMessage("", error, "error");
         }
      );
   }

   validatePassword() {
      if (this.user.password == null || this.user.password.length < 3) {
         this.addMessage(
            "",
            "La contraseña debe contener al menos tres caracteres",
            "warn"
         );
         return false;
      }
      return true;
   }

   addMessage(title: string, message: string, type: string) {
      this.clearMessage();
      this.messageService.add({
         severity: type,
         summary: title,
         detail: message
      });
   }

   clearMessage() {
      this.messageService.clear();
   }
}
