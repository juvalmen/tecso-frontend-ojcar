import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from 'src/services/user/user.service';
import { MessageService } from 'primeng/api';
import { Login } from 'src/interfaces/login.interface';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
  providers: [MessageService]
})
export class SignInComponent implements OnInit {
  isLoginError : boolean = false;
  constructor(private userService: UserService, private router: Router
  ,public messageService: MessageService) { }

  ngOnInit() {
  }

  OnSubmit(userName, password) {
    let user: Login = {
      username: userName,
      password: password
    };
     this.userService.userAuthentication(user).pipe(first()).subscribe(
       (data: any) => {
         console.log(data)
         if (data != "" && data != null) {
           if (data.statusCode == "200") {
             this.router.navigate(["/persona"]);
           } else {
             this.addMessage("", data.responseMessage, 'warn');
           }
         } else { 

         }
        },
        error => {
          this.addMessage("",error,'error'); 
        });
  }

  addMessage(title:string,message:string,type:string) {
    this.clearMessage();
    this.messageService.add({severity:type, summary:title, detail:message});
  }

  clearMessage() {
    this.messageService.clear();
  }

}
