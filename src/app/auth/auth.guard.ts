import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { UrlsEnum } from 'src/model/enum/UrlsEnum';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router : Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean {
      if (localStorage.getItem(UrlsEnum.TOKEN.name) != null)
      return true;
      this.router.navigate(['/login']);
      return false;
  }
}
